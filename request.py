import requests
import json


class PoolApiRequest:
    def __init__(self):
        self.dummy = "foobar"

    def loadJson(self, jsonFile):
        with open("json/" + jsonFile, "r") as json_file:
            data = json.load(json_file)
        print(data)
        return data

    def request_post(self, url, jsonFile):
        headers = {'Content-Type': 'application/json'}
        auth = {
                'client_id': 'develop',
                'client_secret': 'develop',
                'version': '1.1.0'
                }
        r = requests.post(
                url,
                headers=headers,
                params=auth,
                data=json.dumps(jsonFile)
                )
        if r.status_code == 200:
            print("status_code: ", r.status_code, "status_msg: ", r.text)
        else:
            print("status_code: ", r.status_code, "status_msg: ", r.text)

    def post(self, url, jsonFile):
        json = self.loadJson(jsonFile)
        self.request_post(url, json)


class PoolFrontendRequest():
    def __init__(self):
        self.Session = requests.session()
        self.dummy = "foobar"
        self.loginUrl = "http://localhost/drops/webapp/authenticate"
        self.headers = {'Content-Type': 'application/json'}

    def loadJson(self, jsonFile):
        with open("json/" + jsonFile, "r") as json_file:
            data = json.load(json_file)
        print(data)
        return data

    def loginSession(self, session, email, password):
        loginData = {'email': email, 'password': password, 'rememberMe': False}
        jar = ""
        result = session.post(
                allow_redirects=True,
                url=self.loginUrl,
                headers=self.headers,
                data=json.dumps(loginData)
                )

        self.printRequest(result)
        result_Stream = session.get("http://localhost/backend/stream/authenticate/drops?route=/backend/stream/identity&ajax=true")#, allow_redirects=False)
        headers = result.headers
        self.printRequest(result_Stream)
       # if result_Stream.status_code == 303:
       #     url = result_Stream.headers['Location']
       #     result_redirect_1 = session.get(url)
       #     self.printRequest(result_redirect_1)
       #     if result_redirect_1.status_code == 303:
       #         url = result_redirect_2.headers['Location']
       #         result_redirect_2 = session.get(url)
       #         self.printRequest(result_redirect_2)
       #         if result_redirect_2.status_code == 303:
       #             url = result_redirect_3.headers['Location']
       #             result_redirect_3 = session.get(url)
       #             self.printRequest(result_redirect_3)



        
       # newheaders['Cookie'] = headers['Set-Cookie']
       # print(session.headers['Set-Cookie'], "\n")
       # session.headers.update({'Content-Type': 'application/json'})

        #session.headers.update({'X-Requested-With': 'XMLHttpRequest'})
        #result_Stream_2 = session.post("http://localhost/backend/stream/donations")#, headers={'Cookie': headers['Set-Cookie']})
        #self.printRequest(result_Stream_2)

    def postSession(self, session, url, jsonFile):
        session.headers.update({'Content-Type': 'application/json'})
        session.headers.update({'X-Requested-With': 'XMLHttpRequest'})
        data = self.loadJson(jsonFile)
        result = session.post(
            url,
            headers=self.headers,
            data=json.dumps(data)
                )
        self.printRequest(result)
    def printRequest(self, request):
        print('{}\n{}\n{}\n\n{}'.format(
            '-----------Request-----------',
            request.request.method + ' ' + request.request.url,
            '\n'.join('{}: {}'.format(k, v) for k, v in request.request.headers.items()),
            request.request.body,
        ))
        print('\n{}\n{}\n\n{}'.format(
            '-----------Response-----------',
            '\n'.join('{}: {}'.format(k, v) for k, v in request.headers.items()),
            request.text
        ))
       # print( 
       #         "###########\n# Response #\n###########\n",
       #         "status_code: ", request.status_code, "\n", "#####\n",
       #         "cookies: ", request.cookies, "\n", "######\n",
       #         "headers: ", request.headers, "\n", "######\n",
       #         "status_text: ", request.text, "\n", "#####\n",
       #         "history: ", request.history, "\n", "#####\n",
       #         "##################\n# Request Finish #\n##################\n",

       #         )
