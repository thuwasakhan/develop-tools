import sys
import argparse
import requests
from request import PoolApiRequest, PoolFrontendRequest


def main():
    # default argument handler for pool-cli
    parser = argparse.ArgumentParser(prog='pool-cli')
    subparsers = parser.add_subparsers()
    # argument parser for api functions
    api = subparsers.add_parser('api', help="api help")
    api.add_argument(
            '-p', '--post',
            nargs=2,
            metavar=('url', 'jsonFile'),
            help="jsonFile in Json Folder"
            )
    api.set_defaults(which='api')
    # argument parser for frontend functions
    front = subparsers.add_parser('front', help="front help")
    front.add_argument(
            '-l', '--login',
            nargs=2,
            metavar=('email', 'password'),
            help="email and password required"
            )
    front.add_argument(
            '-p', '--post',
            nargs=2,
            metavar=('url', 'jsonFile'),
            help="jsonFile in Json Folder"
            )

    front.set_defaults(which='front')
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
    elif args.which == 'api':
        api_c = PoolApiRequest()
        if args.post is not None:
            api_c.post(args.post[0], args.post[1])
        else:
            api.print_help()
    elif args.which == 'front':
        session = requests.session()
        front_c = PoolFrontendRequest()
        if args.login is not None:
            front_c.loginSession(session, args.login[0], args.login[1])
        if args.post is not None:
            front_c.postSession(session, args.post[0], args.post[1])
        else:
            front.print_help()
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
